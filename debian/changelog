python-sparse (0.16.0a9-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * Standards-Version: 4.7.0 (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Clean sparse/_version.py
  TODO: Segmentation fault on arm64 reported upstream
        https://github.com/pydata/sparse/issues/628
        Moreover Salsa reports test suite issues even for amd64

  [ Roland Mas ]
  * New upstream release, let's see if if fixes the FTBFS on all arches.

 -- Roland Mas <lolando@debian.org>  Wed, 06 Nov 2024 16:14:02 +0100

python-sparse (0.15.1-1) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * New upstream version
    Closes: #1056504
  * Most patches applied upstream
  * Build-Depends: pybuild-plugin-pyproject
  * Build-Depends: python3-pytest-cov

  [ Nilesh Patra ]
  * Build-Depends: python3-setuptools-scm

 -- Andreas Tille <tille@debian.org>  Sat, 13 Jan 2024 15:35:16 +0100

python-sparse (0.14.0-2) unstable; urgency=medium

  * Remove embedded versioneer in preference of debian versioneer package
    using remove-embedded-versioneer.patch
    The embedded versioneer is also incompatble with Python 3.12  (Closes: #1059630)
  * numba, numpy, and scipy are all hard dependencies of sparse so remove
    the <!nocheck> flag from d/control
  * Update fix-test-fails-on-i386.patch for the loss of numpy.distutils.

 -- Diane Trout <diane@ghic.org>  Mon, 01 Jan 2024 15:46:53 -0800

python-sparse (0.14.0-1) unstable; urgency=medium

  [ Bo YU ]
  * Team upload.
  * New upstream release.
    Closes: #988424
  * Rebase d/patches/No-pytest-config.patch.
  * Set std-ver to 4.6.2.
  * Add d/patches/fix-test-fails.patch to fix test failed issue.
  * Fix running testsuite on build. (Closes: #1052388, #1052815)
  * Add d/patches/fix-test-fails-on-i386.patch to fix ftbfs on i386.

  [ Jeroen Ploemen ]
  * Remove unused build-deps on python3-{packaging,pytest-cov,six}.

  [ Andreas Tille ]
  * Build-Depends: s/dh-python/dh-sequence-python3/ (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 07 Nov 2023 09:09:10 +0100

python-sparse (0.13.0-1) UNRELEASED; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Submit.

  [ Diane Trout ]
  * Remove unneeded dpkg-dev build dependency
  * Remove skip-doc-test-on-32bit.patch, applied upstream

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 Aug 2021 02:40:47 -0000

python-sparse (0.12.0-1) unstable; urgency=medium

  * New upstream release
  * Remove unused patch skip-test-repack-on-32bit.patch
  * Remove obsoleted patch use-correct-pointer-to-coo.patch
    - resolved upstream
  * Add new skip-doc-test-on-32bit.patch based on upstream

 -- Diane Trout <diane@ghic.org>  Thu, 19 Aug 2021 22:10:50 -0700

python-sparse (0.11.2-3) unstable; urgency=medium

  * Add use-correct-pointer-to-coo.patch from upstreams fix for 32-bit
    https://github.com/pydata/sparse/pull/421 (Closes: #981615)
    - Remove skip-test-repack-on-32bit.patch since there is a real fix

 -- Diane Trout <diane@ghic.org>  Tue, 02 Feb 2021 09:19:40 -0800

python-sparse (0.11.2-2) unstable; urgency=medium

  * Add skip-test-repack-on-32bit.patch.
    One unit test fails on 32 bit systems it looks like due to a
    type size error. I disabled it so the rest of the 64-bit architectures
    would migrate.
  * Add myself to uploaders

 -- Diane Trout <diane@ghic.org>  Mon, 01 Feb 2021 20:03:00 -0800

python-sparse (0.11.2-1) unstable; urgency=medium

  * Team upload.
  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python3-Version field
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set upstream metadata fields: Repository, Repository-Browse.

  [ Diane Trout ]
  * Update upstream to https://github.com/pydata/sparse as recommended
    by dask developers. https://github.com/dask/dask/issues/7078
  * Add Rules-Requires-Root: no
  * Update upstream maintainer to what's listed on pypi
  * Update Standards version to 4.5.1. No changes required.
  * New upstream release. (Closes: #980317)
  * Add dask and pytest-cov as test dependencies
  * add numba as build dependency
  * Update No-pytest-config patch. The file containing the options moved,
    and upstream's options led to test result files being in the package.
  * Update to debhelper-compat = 13
  * Update upstream urls in debian/upstream/metadata
  * Add issue tracker url to debian/upstream/metadata
  * Indicate No-pytest-config does not need to be forwarded

 -- Diane Trout <diane@ghic.org>  Thu, 21 Jan 2021 16:24:51 -0800

python-sparse (0.2.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Ghislain Antony Vaillant ]
  * New upstream version 0.2.0
  * Drop superfluous nocheck guards
  * Fixup whitespacing in rules file
  * Add versioned dependency to scipy
  * Add new install dependency on six
  * Add new test dependency on packaging
  * Move extend-diff-ignore to d/s/options
  * Add patch disabling the upstream pytest config
    - New patch: No-pytest-config.patch
  * Bump the debhelper version to 11
  * Bump the standards version to 4.1.3
  * Update the copyright years

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Thu, 22 Feb 2018 09:22:45 +0000

python-sparse (0.1.1-1) unstable; urgency=medium

  * New upstream version 0.1.1
  * Ensure tests are run in isolation
  * Bump standards version to 4.0.1, no changes required

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 07 Aug 2017 09:39:55 +0100

python-sparse (0.1.0-1) unstable; urgency=low

  * Initial release. (Closes: #862142)

 -- Ghislain Antony Vaillant <ghisvail@gmail.com>  Mon, 08 May 2017 23:31:10 +0100
